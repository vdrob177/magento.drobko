<?php
namespace Magura\NovaPoshtaShipping\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class RegionActions extends Column
{
    /** Url paths */
    const CITY_URL_PATH = 'novaposhta/cities/';
    const REGION_URL_PATH = 'novaposhta/regions/';

    /** @var UrlInterface */
    protected $urlBuilder;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['ref'])) {
                    $item[$name]['view'] = [
                        'href' => $this->urlBuilder->getUrl(self::CITY_URL_PATH, ['area' => $item['ref']]),
                        'label' => __('View cities')
                    ];
                    $item[$name]['edit'] = [
                        'href' => $this->urlBuilder->getUrl(self::REGION_URL_PATH . 'edit/', ['region_id' => $item['region_id']]),
                        'label' => __('Edit')
                    ];
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(self::REGION_URL_PATH . 'delete/', ['region_id' => $item['region_id']]),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete region'),
                            'message' => __('Are you sure you wan\'t to delete this region?')
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}
