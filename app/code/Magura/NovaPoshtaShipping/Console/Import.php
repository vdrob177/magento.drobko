<?php

namespace Magura\NovaPoshtaShipping\Console;

use Magura\NovaPoshtaShipping\Api\CityRepositoryInterface;
use Magura\NovaPoshtaShipping\Api\Data\CityInterfaceFactory;
use Magura\NovaPoshtaShipping\Api\Data\RegionInterfaceFactory;
use Magura\NovaPoshtaShipping\Api\Data\WarehouseInterfaceFactory;
use Magura\NovaPoshtaShipping\Api\RegionRepositoryInterface;
use Magura\NovaPoshtaShipping\Api\WarehouseRepositoryInterface;
use Magura\NovaPoshtaShipping\Helper\NovaPoshtaResponse;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Import extends Command
{
    /**
     * @var NovaPoshtaResponse
     */
    protected $response;
    /**
     * @var CityRepositoryInterface
     */
    protected $cityRepository;
    /**
     * @var RegionRepositoryInterface
     */
    protected $regionRepository;
    /**
     * @var WarehouseRepositoryInterface
     */
    protected $warehouseRepository;
    protected $cityFactory;
    protected $regionFactory;
    protected $warehouseFactory;

    public function __construct(
        NovaPoshtaResponse $response,
        CityRepositoryInterface $cityRepository,
        WarehouseRepositoryInterface $warehouseRepository,
        RegionRepositoryInterface $regionRepository,
        CityInterfaceFactory $cityFactory,
        WarehouseInterfaceFactory $warehouseFactory,
        RegionInterfaceFactory $regionFactory
    )
    {
        $this->warehouseRepository = $warehouseRepository;
        $this->regionRepository = $regionRepository;
        $this->cityRepository = $cityRepository;
        $this->warehouseFactory = $warehouseFactory;
        $this->cityFactory = $cityFactory;
        $this->regionFactory = $regionFactory;
        $this->response = $response;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('novaposhta:import');
        $this->setDescription('Import Warehouses,Cities and Regions');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Start import');
        $this->setRegions();
        $output->writeln('Import Regions is complete');
        $this->setCities();
        $output->writeln('Import Cities is complete');
        $this->setWarehouse();
        $output->writeln('Import Warehouses is complete');
    }

    protected function setCities()
    {
        $cities = $this->response->response("Address", "getCities")->data;
        if ($cities) {
            $this->cityRepository->truncate();
            foreach ($cities as $city) {
                try {
                    /** @var \Magura\NovaPoshtaShipping\Api\Data\RegionInterface $region */
                    $region = $this->regionRepository->get($city->Area, "ref");
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                    continue;
                }
                /** @var \Magura\NovaPoshtaShipping\Api\Data\CityInterface $cityModel */
                $cityModel = $this->cityFactory->create();
                $cityModel->setDescription($city->Description);
                $cityModel->setDescriptionRu($city->DescriptionRu);
                $cityModel->setRef($city->Ref);
                $cityModel->setMonday($city->Delivery1 === "1");
                $cityModel->setTuesday($city->Delivery2 === "1");
                $cityModel->setWednesday($city->Delivery3 === "1");
                $cityModel->setThursday($city->Delivery4 === "1");
                $cityModel->setFriday($city->Delivery5 === "1");
                $cityModel->setSaturday($city->Delivery6 === "1");
                $cityModel->setSunday($city->Delivery7 === "1");
                $cityModel->setArea($city->Area);
                $cityModel->setAreaId($region->getId());
                $cityModel->setSettlementType($city->SettlementType);
                $cityModel->setIsBranch($city->IsBranch === "1");
                $cityModel->setCityCode($city->CityID);
                $cityModel->setSettlementTypeDescription(isset($city->SettlementTypeDescription) ?
                    $city->SettlementTypeDescription : null);
                $cityModel->setSettlementTypeDescriptionRu(isset($city->SettlementTypeDescriptionRu) ?
                    $city->SettlementTypeDescriptionRu : null);
                $this->cityRepository->save($cityModel);
            }
        }
    }

    public function setRegions()
    {
        $regions = $this->response->response("Address", "getAreas")->data;
        if ($regions) {
            $this->regionRepository->truncate();
            foreach ($regions as $region) {
                /** @var \Magura\NovaPoshtaShipping\Api\Data\RegionInterface $regionModel */
                $regionModel = $this->regionFactory->create();
                $regionModel->setDescription($region->Description);
                $regionModel->setRef($region->Ref);
                $regionModel->setAreasCenter($region->AreasCenter);
                $this->regionRepository->save($regionModel);
            }
        }

    }
    public function setWarehouse()
    {
        $cityWarehouses = $this->response->response(
            "AddressGeneral",
            "getWarehouses"
        )->data;
        if ($cityWarehouses) {
            $this->warehouseRepository->truncate();
            foreach ($cityWarehouses as $warehouse) {
                try {
                    /** @var \Magura\NovaPoshtaShipping\Api\Data\CityInterface $cityModelId */
                    $cityModel = $this->cityRepository->get($warehouse->CityRef, "ref");
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                    continue;
                }
                /** @var \Magura\NovaPoshtaShipping\Api\Data\WarehouseInterfaceFactory $warehouse */
                $addressModel = $this->warehouseFactory->create();
                $addressModel->setDescription($warehouse->Description);
                $addressModel->setDescriptionRu($warehouse->DescriptionRu);
                $addressModel->setRef($warehouse->Ref);
                $addressModel->setWarehouseType($warehouse->TypeOfWarehouse);
                $addressModel->setCityRef($warehouse->CityRef);
                $addressModel->setCityDescription($warehouse->CityDescription);
                $addressModel->setCityDescriptionRu($warehouse->CityDescriptionRu);
                $addressModel->setAreaDescription($warehouse->SettlementAreaDescription);
                $addressModel->setNumber($warehouse->Number);
                $addressModel->setLongitude($warehouse->Longitude);
                $addressModel->setLatitude($warehouse->Latitude);
                $addressModel->setPosTerminal($warehouse->POSTerminal);
                $addressModel->setPostFinance($warehouse->PostFinance);

                $addressModel->setCityId($cityModel->getId());

                $addressModel->setMondayDelivery($warehouse->Delivery->Monday);
                $addressModel->setTuesdayDelivery($warehouse->Delivery->Tuesday);
                $addressModel->setWednesdayDelivery($warehouse->Delivery->Wednesday);
                $addressModel->setThursdayDelivery($warehouse->Delivery->Thursday);
                $addressModel->setFridayDelivery($warehouse->Delivery->Friday);
                $addressModel->setSaturdayDelivery($warehouse->Delivery->Saturday);
                $addressModel->setSundayDelivery($warehouse->Delivery->Sunday);

                $addressModel->setMondayReception($warehouse->Reception->Monday);
                $addressModel->setTuesdayReception($warehouse->Reception->Tuesday);
                $addressModel->setWednesdayReception($warehouse->Reception->Wednesday);
                $addressModel->setThursdayReception($warehouse->Reception->Thursday);
                $addressModel->setFridayReception($warehouse->Reception->Friday);
                $addressModel->setSaturdayReception($warehouse->Reception->Saturday);
                $addressModel->setSundayReception($warehouse->Reception->Sunday);

                $addressModel->setMondaySchedule($warehouse->Schedule->Monday);
                $addressModel->setTuesdaySchedule($warehouse->Schedule->Tuesday);
                $addressModel->setWednesdaySchedule($warehouse->Schedule->Wednesday);
                $addressModel->setThursdaySchedule($warehouse->Schedule->Thursday);
                $addressModel->setFridaySchedule($warehouse->Schedule->Friday);
                $addressModel->setSaturdaySchedule($warehouse->Schedule->Saturday);
                $addressModel->setSundaySchedule($warehouse->Schedule->Sunday);

                $this->warehouseRepository->save($addressModel);
            }
        }
    }
}
