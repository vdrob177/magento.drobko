<?php
namespace Magura\NovaPoshtaShipping\Plugin\Checkout\Model;

use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magura\NovaPoshtaShipping\Api\RegionRepositoryInterface;
use Magura\NovaPoshtaShipping\Api\CityRepositoryInterface;
use Magura\NovaPoshtaShipping\Api\WarehouseRepositoryInterface;

class ShippingInformationManagement
{
    /**
     * @var RegionRepositoryInterface
     */
    protected $regionRepository;

    /**
     * @var CityRepositoryInterface
     */
    protected $cityRepository;

    /**
     * @var WarehouseRepositoryInterface
     */
    protected $warehouseRepository;

    /**
     * ShippingInformationManagement constructor.
     *
     * @param RegionRepositoryInterface $regionRepository
     * @param CityRepositoryInterface $cityRepository
     * @param WarehouseRepositoryInterface $warehouseRepository
     */
    public function __construct(
        RegionRepositoryInterface $regionRepository,
        CityRepositoryInterface $cityRepository,
        WarehouseRepositoryInterface $warehouseRepository
    ) {
        $this->regionRepository = $regionRepository;
        $this->cityRepository = $cityRepository;
        $this->warehouseRepository = $warehouseRepository;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param ShippingInformationInterface $addressInformation
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    ) {
        $shippingCarrier = $addressInformation->getShippingCarrierCode();
        $shippingMethod = $addressInformation->getShippingMethodCode();
        if ($shippingCarrier . '_' . $shippingMethod === 'novaposhta_department') {
            $extAttributes = $addressInformation->getExtensionAttributes();
            $shippingAddress = $addressInformation->getShippingAddress();
            $region = $this->regionRepository->getById($extAttributes->getShippingRegion());
            $city = $this->cityRepository->getById($extAttributes->getShippingCity());
            $address = $this->warehouseRepository->getById($extAttributes->getShippingAddress());
            $shippingAddress->setRegion($region->getDescription() . " обл.");
            $shippingAddress->setCity($city->getDescription());
            $shippingAddress->setStreet($address->getDescription());
            $shippingAddress->setCustomerAddressId(null);
            $shippingAddress->setSaveInAddressBook(0);
        }
    }
}
