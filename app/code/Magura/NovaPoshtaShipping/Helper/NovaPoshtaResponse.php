<?php

namespace Magura\NovaPoshtaShipping\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class NovaPoshtaResponse extends AbstractHelper
{
    const URL_VALUE_PATH = 'carriers/novaposhta/url';
    const API_KEY_VALUE_PATH = 'carriers/novaposhta/key';

    /**
     * GET API KEY NOVA POSHTA
     * @return string
     */
    public function getApiKey()
    {
        return $this->scopeConfig->getValue(self::API_KEY_VALUE_PATH);
    }
    /**
     * GET Url NOVA POSHTA
     * @return string
     */
    public function getUrl()
    {
        return $this->scopeConfig->getValue(self::URL_VALUE_PATH);
    }
    /**
     * @param $modelName
     * @param $calledMethod
     * @param string $methodProperties
     * @return mixed
     */
    public function response($modelName, $calledMethod, $methodProperties = "")
    {
        $httpHeaders = new \Zend\Http\Headers();
        $httpHeaders->addHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ]);
        // request init
        $request = new \Zend\Http\Request();
        $request->setHeaders($httpHeaders);
        $request->setUri($this->getUrl());
        $request->setMethod(\Zend\Http\Request::METHOD_POST);
        $body = '{
"apiKey": "' . $this->getApiKey(). '",
"modelName": "' . $modelName . '",
"calledMethod": "' . $calledMethod . '",
"methodProperties": {
' . $methodProperties . '
    }
}';
        // Zend client init
        $client = new \Zend\Http\Client();
        $options = [
            'adapter' => \Zend\Http\Client\Adapter\Curl::class,
            'curloptions' => [CURLOPT_FOLLOWLOCATION => true],
            'maxredirects' => 0,
            'timeout' => 30
        ];
        $client->setOptions($options);
        $request->setContent($body);

        $response = $client->send($request);

        return (json_decode($response->getBody()));
    }
}
