<?php


namespace Magura\NovaPoshtaShipping\Api\Data;


interface RegionInterface
{
    const REGION_ID ='region_id';
    const DESCRIPTION ='description';
    const REF = 'ref';
    const AREA = 'areas_center';

    /**
     * Get Region Id
     * @return int
     */
    public function getId();

    /**
     * Get Description
     * @return string
     */
    public function getDescription();

    /**
     * Get Ref
     * @return string
     */
    public function getRef();

    /**
     * Get Areas Center
     * @return string
     */
    public function getAreasCenter();

    /**
     * Set Region Id
     * @param $regionId
     * @return int
     */
    public function setId($regionId);

    /**
     * @param $description
     * @return string
     */
    public function setDescription($description);

    /**
     * @param $ref
     * @return string
     */
    public function setRef($ref);

    /**
     * @param $areasCenter
     * @return string
     */
    public function setAreasCenter($areasCenter);
}
