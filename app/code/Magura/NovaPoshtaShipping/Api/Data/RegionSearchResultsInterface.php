<?php

namespace Magura\NovaPoshtaShipping\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for region search results.
 * @api
 */
interface RegionSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get region list.
     *
     * @return \Magura\NovaPoshtaShipping\Api\Data\RegionInterface[]
     */
    public function getItems();

    /**
     * Set region list.
     *
     * @param \Magura\NovaPoshtaShipping\Api\Data\RegionInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
