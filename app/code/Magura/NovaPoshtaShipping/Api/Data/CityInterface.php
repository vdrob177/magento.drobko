<?php


namespace Magura\NovaPoshtaShipping\Api\Data;


interface CityInterface
{
    const CITY_ID = 'city_id';
    const DESCRIPTION = 'description';
    const DESCRIPTION_RU = 'description_ru';
    const AREA = 'area';
    const REF = 'ref';
    const AREA_ID = 'area_id';
    const SETTLEMENT_TYPE = 'settlement_type';
    const IS_BRANCH = 'is_branch';
    const CITY_CODE = 'city_code';
    const SETTLEMENT_TYPE_DESCRIPTION = 'settlement_type_description';
    const SETTLEMENT_TYPE_DESCRIPTION_RU = 'settlement_type_description_ru';
    const MONDAY = 'monday';
    const TUESDAY = 'tuesday';
    const WEDNESDAY = 'wednesday';
    const THURSDAY = 'thursday';
    const FRIDAY = 'friday';
    const SATURDAY = 'saturday';
    const SUNDAY = 'sunday';

    /**
     * Get City Id
     * @return int
     */
    public function getId();

    /**
     * Get Description
     * @return string
     */
    public function getDescription();

    /**
     * Get Description (RU)
     * @return string
     */
    public function getDescriptionRu();

    /**
     * Get Ref
     * @return string
     */
    public function getRef();

    /**
     * Get Area
     * @return string
     */
    public function getArea();

    /**
     * Get area id
     * @return int
     */
    public function getAreaId();

    /**
     * Get Settlement Type
     * @return string
     */
    public function getSettlementType();

    /**
     * Get Is Branch
     * @return string
     */
    public function getIsBranch();

    /**
     * Get City Code
     * @return string
     */
    public function getCityCode();

    /**
     * Get Settlement Type Description
     * @return string
     */
    public function getSettlementTypeDescription();

    /**
     * Get Settlement Type Description (RU)
     * @return string
     */
    public function getSettlementTypeDescriptionRu();

    /**
     * Set City Id
     * @param $cityId
     * @return CityInterface
     */
    public function setId($cityId);

    /**
     * Set Description
     * @param $description
     * @return CityInterface
     */
    public function setDescription($description);

    /**
     * Set Description (RU)
     * @param $description
     * @return CityInterface
     */
    public function setDescriptionRu($description);

    /**
     * Set Ref
     * @param $ref
     * @return CityInterface
     */
    public function setRef($ref);

    /**
     * Set Area
     * @param $area
     * @return CityInterface
     */
    public function setArea($area);

    /**
     * Set area id
     * @param $areaId
     * @return CityInterface
     */
    public function setAreaId($areaId);

    /**
     * Set Settlement Type
     * @param $settlementType
     * @return CityInterface
     */
    public function setSettlementType($settlementType);

    /**
     * Set Is Branch
     * @param $isBranch
     * @return CityInterface
     */
    public function setIsBranch($isBranch);

    /**
     * Set City Code
     * @param $cityCode
     * @return CityInterface
     */
    public function setCityCode($cityCode);

    /**
     * Set Settlement Type Description
     * @param $settlementTypeDescription
     * @return CityInterface
     */
    public function setSettlementTypeDescription($settlementTypeDescription);

    /**
     * Set Settlement Type Description (RU)
     * @param $settlementTypeDescriptionRu
     * @return CityInterface
     */
    public function setSettlementTypeDescriptionRu($settlementTypeDescriptionRu);

    /**
     * Get Monday
     * @return int
     */
    public function getMonday();

    /**
     * Get Tuesday
     * @return int
     */
    public function getTuesday();

    /**
     * Get Wednesday
     * @return int
     */
    public function getWednesday();

    /**
     * Get Thursday
     * @return int
     */
    public function getThursday();

    /**
     * Get Frida
     * @return int
     */
    public function getFriday();

    /**
     * Get Saturday
     * @return int
     */
    public function getSaturday();

    /**
     * Get Sunday
     * @return int
     */
    public function getSunday();

    /**
     * Set Monday
     * @param $day
     * @return CityDeliveryInterface
     */
    public function setMonday($day);

    /**
     * Set Tuesday
     * @param $day
     * @return CityDeliveryInterface
     */
    public function setTuesday($day);

    /**
     * Set Wednesday
     * @param $day
     * @return CityDeliveryInterface
     */
    public function setWednesday($day);

    /**
     * Set Thursday
     * @param $day
     * @return CityDeliveryInterface
     */
    public function setThursday($day);

    /**
     * Set Friday
     * @param $day
     * @return CityDeliveryInterface
     */
    public function setFriday($day);

    /**
     * Set Saturday
     * @param $day
     * @return CityDeliveryInterface
     */
    public function setSaturday($day);

    /**
     * Set Sunday
     * @param $day
     * @return CityDeliveryInterface
     */
    public function setSunday($day);
}
