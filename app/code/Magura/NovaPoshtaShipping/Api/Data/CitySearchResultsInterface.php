<?php

namespace Magura\NovaPoshtaShipping\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for city search results.
 * @api
 */
interface CitySearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get city list.
     *
     * @return \Magura\NovaPoshtaShipping\Api\Data\CityInterface[]
     */
    public function getItems();

    /**
     * Set city list.
     *
     * @param \Magura\NovaPoshtaShipping\Api\Data\CityInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
