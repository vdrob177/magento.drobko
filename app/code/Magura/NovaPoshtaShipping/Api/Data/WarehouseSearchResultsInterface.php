<?php

namespace Magura\NovaPoshtaShipping\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for address search results.
 * @api
 */
interface WarehouseSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get address list.
     *
     * @return \Magura\NovaPoshtaShipping\Api\Data\WarehouseInterface[]
     */
    public function getItems();

    /**
     * Set address list.
     *
     * @param \Magura\NovaPoshtaShipping\Api\Data\WarehouseInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
