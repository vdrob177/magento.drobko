<?php


namespace Magura\NovaPoshtaShipping\Api\Data;



interface WarehouseInterface
{
    const WAREHOUSE_ID = 'warehouse_id';
    const CITY_ID = 'city_id';
    const DESCRIPTION = 'description';
    const DESCRIPTION_RU = 'description_ru';
    const REF = 'ref';
    const NUMBER = 'number';
    const CITY_REF = 'city_ref';
    const CITY_DESCRIPTION = 'city_description';
    const CITY_DESCRIPTION_RU = 'city_description_ru';
    const LONGITUDE = 'long';
    const LATITUDE = 'lat';
    const POST_FINANCE = 'post_finance';
    const POS_TERMINAL = 'pos_terminal';
    const INTERNATIONAL_SHIPPING = 'international_shipping';
    const WAREHOUSE_TYPE = 'warehouse_type';
    const AREA_DESCRIPTION = 'area_description';

    const MONDAY_DELIVERY = 'monday_delivery';
    const TUESDAY_DELIVERY = 'tuesday_delivery';
    const WEDNESDAY_DELIVERY = 'wednesday_delivery';
    const THURSDAY_DELIVERY = 'thursday_delivery';
    const FRIDAY_DELIVERY = 'friday_delivery';
    const SATURDAY_DELIVERY = 'saturday_delivery';
    const SUNDAY_DELIVERY = 'sunday_delivery';

    const MONDAY_RECEPTION = 'monday_reception';
    const TUESDAY_RECEPTION = 'tuesday_reception';
    const WEDNESDAY_RECEPTION = 'wednesday_reception';
    const THURSDAY_RECEPTION = 'thursday_reception';
    const FRIDAY_RECEPTION = 'friday_reception';
    const SATURDAY_RECEPTION = 'saturday_reception';
    const SUNDAY_RECEPTION = 'sunday_reception';

    const MONDAY_SCHEDULE = 'monday_schedule';
    const TUESDAY_SCHEDULE = 'tuesday_schedule';
    const WEDNESDAY_SCHEDULE = 'wednesday_schedule';
    const THURSDAY_SCHEDULE = 'thursday_schedule';
    const FRIDAY_SCHEDULE = 'friday_schedule';
    const SATURDAY_SCHEDULE = 'saturday_schedule';
    const SUNDAY_SCHEDULE = 'sunday_schedule';


    /**
     * Get Warehouse Id
     * @return int
     */
    public function getId();

    /**
     * Get city id
     * @return int
     */
    public function getCityId();
    /**
     * Get Warehouse Description
     * @return string
     */
    public function getDescription();

    /**
     * Get Warehouse Description RU
     * @return string
     */
    public function getDescriptionRu();

    /**
     * Get REF
     * @return string
     */
    public function getRef();

    /**
     * Get Warehouse number
     * @return int
     */
    public function getNumber();

    /**
     * Get City Ref
     * @return string
     */
    public function getCityRef();

    /**
     * Get city Description
     * @return string
     */
    public function getCityDescription();

    /**
     * Get city Description Ru
     * @return string
     */
    public function getCityDescriptionRu();

    /**
     * Get Warehouse Type
     * @return string
     */
    public function getWarehouseType();
    /**
     * Get Longitude
     * @return int
     */
    public function getLongitude();

    /**
     * Get Latitude
     * @return int
     */
    public function getLatitude();

    /**
     * Get Post Finance
     * @return int
     */
    public function getPostFinance();

    /**
     * Get POS Terminal
     * @return int
     */
    public function getPosTerminal();

    /**
     * Get Area Description
     * @return string
     */
    public function getAreaDescription();

    /**
     * Get International Shipping
     * @return int
     */
    public function getInternationalShipping();
    /**
     * Get Delivery Monday
     * @return string
     */
    public function getMondayDelivery();
    /**
     * Get Delivery Tuesday
     * @return string
     */
    public function getTuesdayDelivery();
    /**
     * Get Delivery Wednesday
     * @return string
     */
    public function getWednesdayDelivery();
    /**
     * Get Delivery Thursday
     * @return string
     */
    public function getThursdayDelivery();
    /**
     * Get Delivery Friday
     * @return string
     */
    public function getFridayDelivery();
    /**
     * Get Delivery Saturday
     * @return string
     */
    public function getSaturdayDelivery();
    /**
     * Get Delivery Sunday
     * @return string
     */
    public function getSundayDelivery();
    /**
     * Get Reception Monday
     * @return string
     */
    public function getMondayReception();
    /**
     * Get Reception Tuesday
     * @return string
     */
    public function getTuesdayReception();
    /**
     * Get Reception Wednesday
     * @return string
     */
    public function getWednesdayReception();
    /**
     * Get Reception Thursday
     * @return string
     */
    public function getThursdayReception();
    /**
     * Get Reception Friday
     * @return string
     */
    public function getFridayReception();
    /**
     * Get Reception Saturday
     * @return string
     */
    public function getSaturdayReception();
    /**
     * Get Reception Sunday
     * @return string
     */
    public function getSundayReception();
    /**
     * Get Schedule Monday
     * @return string
     */
    public function getMondaySchedule();
    /**
     * Get Schedule Tuesday
     * @return string
     */
    public function getTuesdaySchedule();
    /**
     * Get Schedule Wednesday
     * @return string
     */
    public function getWednesdaySchedule();
    /**
     * Get Schedule Thursday
     * @return string
     */
    public function getThursdaySchedule();
    /**
     * Get Schedule Friday
     * @return string
     */
    public function getFridaySchedule();
    /**
     * Get Schedule Saturday
     * @return string
     */
    public function getSaturdaySchedule();
    /**
     * Get Schedule Sunday
     * @return string
     */
    public function getSundaySchedule();
    /**
     * Set Warehouse Id
     * @param $warehouseId
     * @return WarehouseInterface
     */
    public function setId($warehouseId);
    /**
     * Set city id
     * @param $cityId
     * @return int
     */
    public function setCityId($cityId);
    /**
     * Set Description
     * @param $description
     * @return WarehouseInterface
     */
    public function setDescription($description);

    /**
     * Set Description Ru
     * @param $descriptionRu
     * @return WarehouseInterface
     */
    public function setDescriptionRu($descriptionRu);

    /**
     * Set ref
     * @param $ref
     * @return WarehouseInterface
     */
    public function setRef($ref);

    /**
     * Set number
     * @param $number
     * @return WarehouseInterface
     */
    public function setNumber($number);

    /**
     * Set city ref
     * @param $cityRef
     * @return WarehouseInterface
     */
    public function setCityRef($cityRef);

    /**
     * Set City Description
     * @param $cityDescription
     * @return WarehouseInterface
     */
    public function setCityDescription($cityDescription);

    /**
     * Set City Description Ru
     * @param $cityDescriptionRu
     * @return WarehouseInterface
     */
    public function setCityDescriptionRu($cityDescriptionRu);

    /**
     * @param $warehouseType
     * @return WarehouseInterface
     */
    public function setWarehouseType($warehouseType);

    /**
     * Set Longitude
     * @param $long
     * @return WarehouseInterface
     */
    public function setLongitude($long);

    /**
     * Set Latitude
     * @param $lat
     * @return WarehouseInterface
     */
    public function setLatitude($lat);

    /**
     * @param $postFinance
     * @return WarehouseInterface
     */
    public function setPostFinance($postFinance);

    /**
     * Set Pos Terminal
     * @param $posTerminal
     * @return WarehouseInterface
     */
    public function setPosTerminal($posTerminal);

    /**
     * Set Area Description
     * @param $areaDescription
     * @return WarehouseInterface
     */
    public function setAreaDescription($areaDescription);
    /**
     * Set International Shipping
     * @param $internationalShipping
     * @return WarehouseInterface
     */
    public function setInternationalShipping($internationalShipping);
    /**
     * Set Monday
     * @param $day
     * @return WarehouseInterface
     */
    public function setMondayDelivery($day);
    /**
     * Set Tuesday
     * @param $day
     * @return WarehouseInterface
     */
    public function setTuesdayDelivery($day);
    /**
     * Set Wednesday
     * @param $day
     * @return WarehouseInterface
     */
    public function setWednesdayDelivery($day);
    /**
     * Set Thursday
     * @param $day
     * @return WarehouseInterface
     */
    public function setThursdayDelivery($day);
    /**
     * Set Friday
     * @param $day
     * @return WarehouseInterface
     */
    public function setFridayDelivery($day);
    /**
     * Set Saturday
     * @param $day
     * @return WarehouseInterface
     */
    public function setSaturdayDelivery($day);
    /**
     * Set Sunday
     * @param $day
     * @return WarehouseInterface
     */
    public function setSundayDelivery($day);
    /**
     * Set Monday Reception
     * @param $day
     * @return WarehouseInterface
     */
    public function setMondayReception($day);
    /**
     * Set Tuesday Reception
     * @param $day
     * @return WarehouseInterface
     */
    public function setTuesdayReception($day);
    /**
     * Set Wednesday Reception
     * @param $day
     * @return WarehouseInterface
     */
    public function setWednesdayReception($day);
    /**
     * Set Thursday Reception
     * @param $day
     * @return WarehouseInterface
     */
    public function setThursdayReception($day);
    /**
     * Set Friday Reception
     * @param $day
     * @return WarehouseInterface
     */
    public function setFridayReception($day);
    /**
     * Set Saturday Reception
     * @param $day
     * @return WarehouseInterface
     */
    public function setSaturdayReception($day);
    /**
     * Set Sunday Reception
     * @param $day
     * @return WarehouseInterface
     */
    public function setSundayReception($day);
    /**
     * Set Monday Schedule
     * @param $day
     * @return WarehouseInterface
     */
    public function setMondaySchedule($day);
    /**
     * Set Tuesday Schedule
     * @param $day
     * @return WarehouseInterface
     */
    public function setTuesdaySchedule($day);
    /**
     * Set Wednesday Schedule
     * @param $day
     * @return WarehouseInterface
     */
    public function setWednesdaySchedule($day);
    /**
     * Set Thursday Schedule
     * @param $day
     * @return WarehouseInterface
     */
    public function setThursdaySchedule($day);
    /**
     * Set Friday Schedule
     * @param $day
     * @return WarehouseInterface
     */
    public function setFridaySchedule($day);
    /**
     * Set Saturday Schedule
     * @param $day
     * @return WarehouseInterface
     */
    public function setSaturdaySchedule($day);
    /**
     * Set Sunday Schedule
     * @param $day
     * @return WarehouseInterface
     */
    public function setSundaySchedule($day);
}
