<?php

namespace Magura\NovaPoshtaShipping\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * City CRUD interface.
 * @api
 */
interface CityRepositoryInterface
{
    /**
     * Save city.
     *
     * @param \Magura\NovaPoshtaShipping\Api\Data\CityInterface $city
     * @return \Magura\NovaPoshtaShipping\Api\Data\CityInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Magura\NovaPoshtaShipping\Api\Data\CityInterface $city);

    /**
     * Retrieve city.
     *
     * @param int $Id
     * @return \Magura\NovaPoshtaShipping\Api\Data\CityInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($Id);


    /**
     * Delete city.
     *
     * @param \Magura\NovaPoshtaShipping\Api\Data\CityInterface $city
     * @return bool true on success
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Magura\NovaPoshtaShipping\Api\Data\CityInterface $city);

    /**
     * Retrieve city matching the specified criteria.
     *
     * @api
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magura\NovaPoshtaShipping\Api\Data\CitySearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
    /**
     * Delete city by ID.
     *
     * @param int $Id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($Id);

    /**
     * Truncate table
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return null
     */
    public function truncate();

    /**
     * Get city by field
     * @param mixed $value
     * @param string $field field to load by (defaults to model id)
     * @return \Magura\NovaPoshtaShipping\Api\Data\CityInterface
     */
    public function get($value, $field = null);
}
