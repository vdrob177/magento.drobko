<?php

namespace Magura\NovaPoshtaShipping\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Region CRUD interface.
 * @api
 */
interface RegionRepositoryInterface
{
    /**
     * Save region.
     *
     * @param \Magura\NovaPoshtaShipping\Api\Data\RegionInterface  $region
     * @return \Magura\NovaPoshtaShipping\Api\Data\RegionInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Magura\NovaPoshtaShipping\Api\Data\RegionInterface $region);

    /**
     * Retrieve region by id.
     *
     * @param int $Id
     * @return \Magura\NovaPoshtaShipping\Api\Data\RegionInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($Id);

    /**
     * Delete region.
     *
     * @param \Magura\NovaPoshtaShipping\Api\Data\RegionInterface  $region
     * @return bool true on success
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Magura\NovaPoshtaShipping\Api\Data\RegionInterface  $region);
    /**
     * Retrieve region matching the specified criteria.
     *
     * @api
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magura\NovaPoshtaShipping\Api\Data\RegionSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
    /**
     * Delete region by ID.
     *
     * @param int $Id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($Id);

    /**
     * Truncate table
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return null
     */
    public function truncate();

    /**
     * Get region by field
     * @param mixed $value
     * @param string $field field to load by (defaults to model id)
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @return \Magura\NovaPoshtaShipping\Api\Data\RegionInterface
     */
    public function get($value, $field = null);
}
