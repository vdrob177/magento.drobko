<?php

namespace Magura\NovaPoshtaShipping\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Warehouses CRUD interface.
 * @api
 */
interface WarehouseRepositoryInterface
{
    /**
     * Save warehouse.
     *
     * @param \Magura\NovaPoshtaShipping\Api\Data\WarehouseInterface $warehouse
     * @return \Magura\NovaPoshtaShipping\Api\Data\WarehouseInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Magura\NovaPoshtaShipping\Api\Data\WarehouseInterface $warehouse);

    /**
     * Retrieve warehouse.
     *
     * @param int $Id
     * @return \Magura\NovaPoshtaShipping\Api\Data\WarehouseInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($Id);


    /**
     * Delete warehouse.
     *
     * @param \Magura\NovaPoshtaShipping\Api\Data\WarehouseInterface $warehouse
     * @return bool true on success
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Magura\NovaPoshtaShipping\Api\Data\WarehouseInterface $warehouse);
    /**
     * Retrieve warehouses matching the specified criteria.
     *
     * @api
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magura\NovaPoshtaShipping\Api\Data\WarehouseSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
    /**
     * Delete warehouse by ID.
     *
     * @param int $Id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($Id);

    /**
     * Truncate table
     * @return null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function truncate();

    /**
     * Get warehouse by field
     * @param mixed $value
     * @param string $field field to load by (defaults to model id)
     * @return \Magura\NovaPoshtaShipping\Api\Data\WarehouseInterface
     */
    public function get($value, $field = null);
}
