<?php
namespace Magura\NovaPoshtaShipping\Block\Adminhtml\Region\Edit;

use Magento\Backend\Block\Widget\Context;
use Magura\NovaPoshtaShipping\Api\RegionRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Generic button for Region edit form
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var RegionRepositoryInterface
     */
    protected $regionRepository;

    /**
     * @param Context $context
     * @param RegionRepositoryInterface $regionRepository
     */
    public function __construct(
        Context $context,
        RegionRepositoryInterface $regionRepository
    ) {
        $this->context = $context;
        $this->regionRepository = $regionRepository;
    }

    /**
     * Return Region ID
     *
     * @return int|null
     */
    public function getId()
    {
        try {
            return $this->regionRepository->getById(
                $this->context->getRequest()->getParam('region_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
