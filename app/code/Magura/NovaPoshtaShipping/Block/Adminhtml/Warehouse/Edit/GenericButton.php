<?php
namespace Magura\NovaPoshtaShipping\Block\Adminhtml\Warehouse\Edit;

use Magento\Backend\Block\Widget\Context;
use Magura\NovaPoshtaShipping\Api\WarehouseRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Generic button for Warehouse edit form
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var WarehouseRepositoryInterface
     */
    protected $warehouseRepository;

    /**
     * @param Context $context
     * @param WarehouseRepositoryInterface $warehouseRepository
     */
    public function __construct(
        Context $context,
        WarehouseRepositoryInterface $warehouseRepository
    ) {
        $this->context = $context;
        $this->warehouseRepository = $warehouseRepository;
    }

    /**
     * Return Warehouse ID
     *
     * @return int|null
     */
    public function getId()
    {
        try {
            return $this->warehouseRepository->getById(
                $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
