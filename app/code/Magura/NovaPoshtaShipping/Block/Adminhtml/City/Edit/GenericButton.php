<?php
namespace Magura\NovaPoshtaShipping\Block\Adminhtml\City\Edit;

use Magento\Backend\Block\Widget\Context;
use Magura\NovaPoshtaShipping\Api\CityRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Generic button for City edit form
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var CityRepositoryInterface
     */
    protected $cityRepository;

    /**
     * @param Context $context
     * @param CityRepositoryInterface $cityRepository
     */
    public function __construct(
        Context $context,
        CityRepositoryInterface $cityRepository
    ) {
        $this->context = $context;
        $this->cityRepository = $cityRepository;
    }

    /**
     * Return City ID
     *
     * @return int|null
     */
    public function getId()
    {
        try {
            return $this->cityRepository->getById(
                $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
