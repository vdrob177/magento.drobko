<?php

namespace Magura\NovaPoshtaShipping\Model;

use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magura\NovaPoshtaShipping\Api\CityRepositoryInterface;
use Magura\NovaPoshtaShipping\Api\Data;
use Magura\NovaPoshtaShipping\Model\ResourceModel\City as CityResource;
use Magura\NovaPoshtaShipping\Model\ResourceModel\City\CollectionFactory as CityCollectionFactory;

/**
 * Repository for Cities
 * Class CityRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CityRepository implements CityRepositoryInterface
{
    /**
     * @var CityResource
     */
    protected $resource;

    /**
     * @var Data\CityInterfaceFactory
     */
    protected $cityFactory;

    /**
     * @var CityCollectionFactory
     */
    protected $cityCollectionFactory;

    /**
     * @var Data\CitySearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param CityResource $resource
     * @param Data\CityInterfaceFactory $cityFactory
     * @param Data\CityInterfaceFactory $dataCityFactory
     * @param CityCollectionFactory $cityCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        CityResource $resource,
        Data\CityInterfaceFactory $cityFactory,
        CityCollectionFactory $cityCollectionFactory,
        Data\CitySearchResultsInterfaceFactory $citySearchResultsInterfaceFactory,
        CollectionProcessorInterface $collectionProcessor = null
    ) {
        $this->resource = $resource;
        $this->cityFactory = $cityFactory;
        $this->cityCollectionFactory = $cityCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $citySearchResultsInterfaceFactory;
    }

    /**
     * Save City data
     *
     * @param Data\CityInterface $city
     * @return Data\CityInterface
     * @throws CouldNotSaveException
     */
    public function save(\Magura\NovaPoshtaShipping\Api\Data\CityInterface $city)
    {
        try {
            $this->resource->save($city);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save city: %1', $exception->getMessage()),
                $exception
            );
        }
        return $city;
    }

    /**
     * Load City data by given City Identity
     *
     * @param string $Id
     * @return Data\CityInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($Id)
    {
        $city = $this->cityFactory->create();
        $this->resource->load($city, $Id);
        if (!$city->getId()) {
            throw new NoSuchEntityException(__('The City with the "%1" ID doesn\'t exist.', $Id));
        }
        return $city;
    }

    /**
     * Delete City
     *
     * @param \Magura\NovaPoshtaShipping\Api\Data\CityInterface $city
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(\Magura\NovaPoshtaShipping\Api\Data\CityInterface $city)
    {
        try {
            $this->resource->delete($city);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete city: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * Delete City by given City Identity
     *
     * @param string $Id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($Id)
    {
        return $this->delete($this->getById($Id));
    }

    /**
     * Truncate table
     * @return null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function truncate()
    {
        /** @var \Magento\Framework\DB\Adapter\AdapterInterface $connection */
        $connection = $this->resource->getConnection();
        $tableName = $this->resource->getMainTable();
        $connection->truncateTable($tableName);
    }

    /**
     * Get city by field
     * @param mixed $value
     * @param string $field field to load by (defaults to model id)
     * @return \Magura\NovaPoshtaShipping\Api\Data\CityInterface
     * @throws NoSuchEntityException
     */
    public function get($value, $field = null)
    {
        $city = $this->cityFactory->create();
        $this->resource->load($city, $value, $field);
        if (!$city->getId()) {
            $fieldName = $field ? $field : Data\CityInterface::CITY_ID;
            throw new NoSuchEntityException(__(
                'The City with data "%1" in field "%2" doesn\'t exist.',
                $value,
                $fieldName
            ));
        }
        return $city;
    }
    /**
     * Load City data collection by given search criteria
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \Magura\NovaPoshtaShipping\Api\Data\CitySearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Magura\NovaPoshtaShipping\Model\ResourceModel\City\Collection $collection */
        $collection = $this->cityCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var Data\CitySearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }
}
