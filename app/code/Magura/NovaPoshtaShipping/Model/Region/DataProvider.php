<?php

namespace Magura\NovaPoshtaShipping\Model\Region;

use Magura\NovaPoshtaShipping\Model\ResourceModel\Region\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $_loadedData;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $regionCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $regionCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $region) {
            $this->_loadedData[$region->getId()] = $region->getData();
        }
        return $this->_loadedData;
    }
}
