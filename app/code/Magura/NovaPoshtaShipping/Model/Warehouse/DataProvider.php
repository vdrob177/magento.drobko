<?php

namespace Magura\NovaPoshtaShipping\Model\Warehouse;

use Magura\NovaPoshtaShipping\Model\ResourceModel\Warehouse\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $_zloadedData;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $warehouse) {
            $this->_loadedData[$warehouse->getId()] = $warehouse->getData();
        }
        return $this->_loadedData;
    }
}
