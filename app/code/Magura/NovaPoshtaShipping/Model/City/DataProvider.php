<?php

namespace Magura\NovaPoshtaShipping\Model\City;

use  Magura\NovaPoshtaShipping\Model\ResourceModel\City\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $_zloadedData;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $cityCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $cityCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $city) {
            $this->_loadedData[$city->getId()] = $city->getData();
        }
        return $this->_loadedData;
    }
}
