<?php

namespace Magura\NovaPoshtaShipping\Model;

use Magura\NovaPoshtaShipping\Api\Data;

use Magura\NovaPoshtaShipping\Api\RegionRepositoryInterface;
use Magura\NovaPoshtaShipping\Model\ResourceModel\Region as RegionResource;
use Magura\NovaPoshtaShipping\Model\ResourceModel\Region\CollectionFactory as RegionCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Repository for Regions
 * Class RegionRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class RegionRepository implements RegionRepositoryInterface
{
    /**
     * @var RegionResource
     */
    protected $resource;

    /**
     * @var Data\RegionInterfaceFactory
     */
    protected $regionFactory;

    /**
     * @var RegionCollectionFactory
     */
    protected $regionCollectionFactory;
    /**
     * @var Data\RegionSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param RegionResource $resource
     * @param Data\RegionInterfaceFactory $regionFactory
     * @param RegionCollectionFactory $regionCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        RegionResource $resource,
        Data\RegionInterfaceFactory $regionFactory,
        RegionCollectionFactory $regionCollectionFactory,
        Data\RegionSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor = null
    ) {
        $this->resource = $resource;
        $this->regionFactory = $regionFactory;
        $this->regionCollectionFactory = $regionCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * Save Region data
     *
     * @param Data\RegionInterface $region
     * @return Data\RegionInterface
     * @throws CouldNotSaveException
     */
    public function save(\Magura\NovaPoshtaShipping\Api\Data\RegionInterface $region)
    {
        try {
            $this->resource->save($region);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save region: %1', $exception->getMessage()),
                $exception
            );
        }
        return $region;
    }

    /**
     * Load Region data by given Region Identity
     *
     * @param string $Id
     * @return Data\RegionInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($Id)
    {
        $region = $this->regionFactory->create();
        $this->resource->load($region, $Id);
        if (!$region->getId()) {
            throw new NoSuchEntityException(__('The Region with the "%1" ID doesn\'t exist.', $Id));
        }
        return $region;
    }
    /**
     * Load Region data collection by given search criteria
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \Magura\NovaPoshtaShipping\Api\Data\RegionSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Magura\NovaPoshtaShipping\Model\ResourceModel\Region\Collection $collection */
        $collection = $this->regionCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var Data\RegionSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }
    /**
     * Delete Region
     *
     * @param Data\RegionInterface $region
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(\Magura\NovaPoshtaShipping\Api\Data\RegionInterface $region)
    {
        try {
            $this->resource->delete($region);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete region: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * Delete Region by given Region Identity
     *
     * @param string $Id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($Id)
    {
        return $this->delete($this->getById($Id));
    }

    /**
     * Truncate table
     * $return null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function truncate()
    {
        /** @var \Magento\Framework\DB\Adapter\AdapterInterface $connection */
        $connection = $this->resource->getConnection();
        $tableName = $this->resource->getMainTable();
        $connection->truncateTable($tableName);
    }

    /**
     * Get region by field
     * @param mixed $value
     * @param string $field field to load by (defaults to model id)
     * @return Data\RegionInterface
     * @throws NoSuchEntityException
     */
    public function get($value, $field = null)
    {
        $region = $this->regionFactory->create();
        $this->resource->load($region, $value, $field);
        if (!$region->getId()) {
            $fieldName = $field ? $field : Data\RegionInterface::REGION_ID;
            throw new NoSuchEntityException(__(
                'The Region with data "%1" in field "%2" doesn\'t exist.',
                $value,
                $fieldName
            ));
        }
        return $region;
    }
}
