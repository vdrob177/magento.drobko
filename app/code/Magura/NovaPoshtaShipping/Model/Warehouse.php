<?php

namespace Magura\NovaPoshtaShipping\Model;

use Magento\Framework\Model\AbstractModel;
use Magura\NovaPoshtaShipping\Api\Data\WarehouseInterface;

class Warehouse extends AbstractModel implements WarehouseInterface
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init(\Magura\NovaPoshtaShipping\Model\ResourceModel\Warehouse::class);
    }
    public function getId()
    {
        return $this->getData(self::WAREHOUSE_ID);
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @inheritDoc
     */
    public function getDescriptionRu()
    {
        return $this->getData(self::DESCRIPTION_RU);
    }

    /**
     * @inheritDoc
     */
    public function getRef()
    {
        return $this->getData(self::REF);
    }

    /**
     * @inheritDoc
     */
    public function getNumber()
    {
        return $this->getData(self::NUMBER);
    }

    /**
     * @inheritDoc
     */
    public function getCityRef()
    {
        return $this->getData(self::CITY_REF);
    }

    /**
     * @inheritDoc
     */
    public function getCityDescription()
    {
        return $this->getData(self::CITY_DESCRIPTION);
    }

    /**
     * @inheritDoc
     */
    public function getCityDescriptionRu()
    {
        return $this->getData(self::CITY_DESCRIPTION_RU);
    }

    /**
     * @inheritDoc
     */
    public function getLongitude()
    {
        return $this->getData(self::LONGITUDE);
    }

    /**
     * @inheritDoc
     */
    public function getLatitude()
    {
        return $this->getData(self::LATITUDE);
    }

    /**
     * @inheritDoc
     */
    public function getPostFinance()
    {
        return $this->getData(self::POST_FINANCE);
    }

    /**
     * @inheritDoc
     */
    public function getPosTerminal()
    {
        return $this->getData(self::POS_TERMINAL);
    }

    /**
     * @inheritDoc
     */
    public function getInternationalShipping()
    {
        return $this->getData(self::INTERNATIONAL_SHIPPING);
    }

    /**
     * @inheritDoc
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @inheritDoc
     */
    public function setDescriptionRu($descriptionRu)
    {
        return $this->setData(self::DESCRIPTION_RU, $descriptionRu);
    }

    /**
     * @inheritDoc
     */
    public function setRef($ref)
    {
        return $this->setData(self::REF, $ref);
    }

    /**
     * @inheritDoc
     */
    public function setNumber($number)
    {
        return $this->setData(self::NUMBER, $number);
    }

    /**
     * @inheritDoc
     */
    public function setCityRef($cityRef)
    {
        return $this->setData(self::CITY_REF, $cityRef);
    }

    /**
     * @inheritDoc
     */
    public function setCityDescription($cityDescription)
    {
        return $this->setData(self::CITY_DESCRIPTION, $cityDescription);
    }

    /**
     * @inheritDoc
     */
    public function setCityDescriptionRu($cityDescriptionRu)
    {
        return $this->setData(self::CITY_DESCRIPTION_RU, $cityDescriptionRu);
    }

    /**
     * @inheritDoc
     */
    public function setLongitude($long)
    {
        return $this->setData(self::LONGITUDE, $long);
    }

    /**
     * @inheritDoc
     */
    public function setLatitude($lat)
    {
        return $this->setData(self::LATITUDE, $lat);
    }

    /**
     * @inheritDoc
     */
    public function setPostFinance($postFinance)
    {
        return $this->setData(self::POST_FINANCE, $postFinance);
    }

    /**
     * @inheritDoc
     */
    public function setPosTerminal($posTerminal)
    {
        return $this->setData(self::POS_TERMINAL, $posTerminal);
    }

    /**
     * @inheritDoc
     */
    public function setInternationalShipping($internationalShipping)
    {
        return $this->setData(self::INTERNATIONAL_SHIPPING, $internationalShipping);
    }

    public function getMondayDelivery()
    {
        return $this->getData(self::MONDAY_DELIVERY);
    }

    public function getTuesdayDelivery()
    {
        return $this->getData(self::TUESDAY_DELIVERY);
    }

    public function getWednesdayDelivery()
    {
        return $this->getData(self::WEDNESDAY_DELIVERY);
    }

    public function getThursdayDelivery()
    {
        return $this->getData(self::THURSDAY_DELIVERY);
    }

    public function getFridayDelivery()
    {
        return $this->getData(self::FRIDAY_DELIVERY);
    }

    public function getSaturdayDelivery()
    {
        return $this->getData(self::SATURDAY_DELIVERY);
    }

    public function getSundayDelivery()
    {
        return $this->getData(self::SUNDAY_DELIVERY);
    }

    public function getMondayReception()
    {
        return $this->getData(self::MONDAY_RECEPTION);
    }

    public function getTuesdayReception()
    {
        return $this->getData(self::TUESDAY_RECEPTION);
    }

    public function getWednesdayReception()
    {
        return $this->getData(self::WEDNESDAY_RECEPTION);
    }

    public function getThursdayReception()
    {
        return $this->getData(self::THURSDAY_RECEPTION);
    }

    public function getFridayReception()
    {
        return $this->getData(self::FRIDAY_RECEPTION);
    }

    public function getSaturdayReception()
    {
        return $this->getData(self::SATURDAY_RECEPTION);
    }

    public function getSundayReception()
    {
        return $this->getData(self::SUNDAY_RECEPTION);
    }

    public function getMondaySchedule()
    {
        return $this->getData(self::MONDAY_SCHEDULE);
    }

    public function getTuesdaySchedule()
    {
        return $this->getData(self::TUESDAY_SCHEDULE);
    }

    public function getWednesdaySchedule()
    {
        return $this->getData(self::WEDNESDAY_SCHEDULE);
    }

    public function getThursdaySchedule()
    {
        return $this->getData(self::THURSDAY_SCHEDULE);
    }

    public function getFridaySchedule()
    {
        return $this->getData(self::FRIDAY_SCHEDULE);
    }

    public function getSaturdaySchedule()
    {
        return $this->getData(self::SATURDAY_SCHEDULE);
    }

    public function getSundaySchedule()
    {
        return $this->getData(self::SUNDAY_SCHEDULE);
    }

    public function setMondayDelivery($day)
    {
        return $this->setData(self::MONDAY_DELIVERY, $day);
    }

    public function setTuesdayDelivery($day)
    {
        return $this->setData(self::TUESDAY_DELIVERY, $day);
    }

    public function setWednesdayDelivery($day)
    {
        return $this->setData(self::WEDNESDAY_DELIVERY, $day);
    }

    public function setThursdayDelivery($day)
    {
        return $this->setData(self::THURSDAY_DELIVERY, $day);
    }

    public function setFridayDelivery($day)
    {
        return $this->setData(self::FRIDAY_DELIVERY, $day);
    }

    public function setSaturdayDelivery($day)
    {
        return $this->setData(self::SATURDAY_DELIVERY, $day);
    }

    public function setSundayDelivery($day)
    {
        return $this->setData(self::SUNDAY_DELIVERY, $day);
    }

    public function setMondayReception($day)
    {
        return $this->setData(self::MONDAY_RECEPTION, $day);
    }

    public function setTuesdayReception($day)
    {
        return $this->setData(self::TUESDAY_RECEPTION, $day);
    }

    public function setWednesdayReception($day)
    {
        return $this->setData(self::WEDNESDAY_RECEPTION, $day);
    }

    public function setThursdayReception($day)
    {
        return $this->setData(self::THURSDAY_RECEPTION, $day);
    }

    public function setFridayReception($day)
    {
        return $this->setData(self::FRIDAY_RECEPTION, $day);
    }

    public function setSaturdayReception($day)
    {
        return $this->setData(self::SATURDAY_RECEPTION, $day);
    }

    public function setSundayReception($day)
    {
        return $this->setData(self::SUNDAY_RECEPTION, $day);
    }

    public function setMondaySchedule($day)
    {
        return $this->setData(self::MONDAY_SCHEDULE, $day);
    }

    public function setTuesdaySchedule($day)
    {
        return $this->setData(self::TUESDAY_SCHEDULE, $day);
    }

    public function setWednesdaySchedule($day)
    {
        return $this->setData(self::WEDNESDAY_SCHEDULE, $day);
    }

    public function setThursdaySchedule($day)
    {
        return $this->setData(self::THURSDAY_SCHEDULE, $day);
    }

    public function setFridaySchedule($day)
    {
        return $this->setData(self::FRIDAY_SCHEDULE, $day);
    }

    public function setSaturdaySchedule($day)
    {
        return $this->setData(self::SATURDAY_SCHEDULE, $day);
    }

    public function setSundaySchedule($day)
    {
        return $this->setData(self::SUNDAY_SCHEDULE, $day);
    }

    public function getWarehouseType()
    {
        return $this->getData(self::WAREHOUSE_TYPE);
    }

    public function setWarehouseType($warehouseType)
    {
        return $this->setData(self::WAREHOUSE_TYPE, $warehouseType);
    }

    public function getAreaDescription()
    {
        return $this->getData(self::AREA_DESCRIPTION);
    }

    public function setAreaDescription($areaDescription)
    {
        return $this->setData(self::AREA_DESCRIPTION, $areaDescription);
    }

    public function getCityId()
    {
        return $this->getData(self::CITY_ID);
    }

    public function setCityId($cityId)
    {
        return $this->setData(self::CITY_ID, $cityId);
    }
}
