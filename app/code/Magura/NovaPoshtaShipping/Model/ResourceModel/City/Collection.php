<?php
namespace Magura\NovaPoshtaShipping\Model\ResourceModel\City;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'city_id';
    protected $_eventPrefix = 'novaposhta_city_collection';
    protected $_eventObject = 'city_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Magura\NovaPoshtaShipping\Model\City::class, \Magura\NovaPoshtaShipping\Model\ResourceModel\City::class);
    }
}
