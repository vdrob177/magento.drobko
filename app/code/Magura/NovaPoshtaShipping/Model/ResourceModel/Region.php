<?php

namespace Magura\NovaPoshtaShipping\Model\ResourceModel;

class Region extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('novaposhta_regions', 'region_id');
    }
}
