<?php


namespace Magura\NovaPoshtaShipping\Model\ResourceModel\Region;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'region_id';
    protected $_eventPrefix = 'novaposhta_region_collection';
    protected $_eventObject = 'region_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Magura\NovaPoshtaShipping\Model\Region::class, \Magura\NovaPoshtaShipping\Model\ResourceModel\Region::class);
    }
}
