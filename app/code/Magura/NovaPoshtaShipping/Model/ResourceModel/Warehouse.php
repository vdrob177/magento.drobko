<?php

namespace Magura\NovaPoshtaShipping\Model\ResourceModel;

class Warehouse extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('novaposhta_warehouses', 'warehouse_id');
    }
}
