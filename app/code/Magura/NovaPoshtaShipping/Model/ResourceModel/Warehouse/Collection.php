<?php
namespace Magura\NovaPoshtaShipping\Model\ResourceModel\Warehouse;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'warehouse_id';
    protected $_eventPrefix = 'novaposhta_warehouse_collection';
    protected $_eventObject = 'warehouse_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Magura\NovaPoshtaShipping\Model\Warehouse::class, \Magura\NovaPoshtaShipping\Model\ResourceModel\Warehouse::class);
    }
}
