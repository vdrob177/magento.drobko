<?php

namespace Magura\NovaPoshtaShipping\Model;

use Magento\Framework\Model\AbstractModel;
use Magura\NovaPoshtaShipping\Api\Data\CityInterface;

class City extends AbstractModel implements CityInterface
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init(\Magura\NovaPoshtaShipping\Model\ResourceModel\City::class);
    }
    public function getId()
    {
        return $this->getData(self::CITY_ID);
    }

    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    public function getDescriptionRu()
    {
        return $this->getData(self::DESCRIPTION_RU);
    }

    public function getArea()
    {
        return $this->getData(self::AREA);
    }

    public function getSettlementType()
    {
        return $this->getData(self::SETTLEMENT_TYPE);
    }

    public function getIsBranch()
    {
        return $this->getData(self::IS_BRANCH);
    }

    public function getCityCode()
    {
        return $this->getData(self::CITY_CODE);
    }

    public function getSettlementTypeDescription()
    {
        return $this->getData(self::SETTLEMENT_TYPE_DESCRIPTION);
    }

    public function getSettlementTypeDescriptionRu()
    {
        return $this->getData(self::SETTLEMENT_TYPE_DESCRIPTION_RU);
    }
    public function setId($cityId)
    {
        return $this->setData(self::CITY_ID, $cityId);
    }

    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    public function setDescriptionRu($description)
    {
        return $this->setData(self::DESCRIPTION_RU, $description);
    }

    public function setArea($area)
    {
        return $this->setData(self::AREA, $area);
    }

    public function setSettlementType($settlementType)
    {
        return $this->setData(self::SETTLEMENT_TYPE, $settlementType);
    }

    public function setIsBranch($isBranch)
    {
        return $this->setData(self::IS_BRANCH, $isBranch);
    }

    public function setCityCode($cityCode)
    {
        return $this->setData(self::CITY_CODE, $cityCode);
    }

    public function setSettlementTypeDescription($settlementTypeDescription)
    {
        return $this->setData(self::SETTLEMENT_TYPE_DESCRIPTION, $settlementTypeDescription);
    }

    public function setSettlementTypeDescriptionRu($settlementTypeDescriptionRu)
    {
        return $this->setData(self::SETTLEMENT_TYPE_DESCRIPTION_RU, $settlementTypeDescriptionRu);
    }

    public function getMonday()
    {
        return $this->getData(self::MONDAY);
    }

    public function getTuesday()
    {
        return $this->getData(self::TUESDAY);
    }

    public function getWednesday()
    {
        return $this->getData(self::WEDNESDAY);
    }

    public function getThursday()
    {
        return $this->getData(self::THURSDAY);
    }

    public function getFriday()
    {
        return $this->getData(self::FRIDAY);
    }

    public function getSaturday()
    {
        return $this->getData(self::SATURDAY);
    }

    public function getSunday()
    {
        return $this->getData(self::SUNDAY);
    }

    public function setMonday($day)
    {
        return $this->setData(self::MONDAY, $day);
    }

    public function setTuesday($day)
    {
        return $this->setData(self::TUESDAY, $day);
    }

    public function setWednesday($day)
    {
        return $this->setData(self::WEDNESDAY, $day);
    }

    public function setThursday($day)
    {
        return $this->setData(self::THURSDAY, $day);
    }

    public function setFriday($day)
    {
        return $this->setData(self::FRIDAY, $day);
    }

    public function setSaturday($day)
    {
        return $this->setData(self::SATURDAY, $day);
    }

    public function setSunday($day)
    {
        return $this->setData(self::SUNDAY, $day);
    }

    public function getRef()
    {
        return $this->getData(self::REF);
    }

    public function getAreaId()
    {
        return $this->getData(self::AREA_ID);
    }

    public function setRef($ref)
    {
        return $this->setData(self::REF,$ref);
    }

    public function setAreaId($areaId)
    {
        return $this->setData(self::AREA_ID,$areaId);
    }
}
