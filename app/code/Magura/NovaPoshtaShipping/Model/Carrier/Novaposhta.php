<?php

namespace Magura\NovaPoshtaShipping\Model\Carrier;

use Magento\Framework\HTTP\ClientFactory;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magura\NovaPoshtaShipping\Helper\Config;

/**
 * Custom shipping model
 */
class Novaposhta extends AbstractCarrier implements CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'novaposhta';

    /**
     * Urls for shipment
     *
     * @var string
     */
    protected $_url = 'https://api.novaposhta.ua/v2.0/json/';

    /**
     * @var ClientFactory
     */
    private $httpClientFactory;

    /**
     * @var bool
     */
    protected $_isFixed = true;

    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    private $rateResultFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    private $rateMethodFactory;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param ClientFactory $httpClientFactory
     * @param Config $configHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        ClientFactory $httpClientFactory,
        Config $configHelper,
        array $data = []
    ) {
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);

        $this->httpClientFactory = $httpClientFactory;
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->configHelper = $configHelper;
    }

    /**
     * Custom Shipping Rates Collector
     *
     * @param RateRequest $request
     * @return \Magento\Shipping\Model\Rate\Result|bool
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->rateResultFactory->create();

        $allowed = explode(',', $this->getConfigData('allowed_methods'));

        foreach ($allowed as $code) {
            /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $rate */
            $rate = $this->rateMethodFactory->create();
            $rate->setCarrier($this->_code);
            $rate->setCarrierTitle($this->getConfigData('title'));
            $rate->setMethod($code);
            $rate->setMethodTitle($this->configHelper->getCode('method', $code));
            $result->append($rate);
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        $allowed = explode(',', $this->getConfigData('allowed_methods'));
        $arr = [];
        foreach ($allowed as $code) {
            $arr[$code] =  $this->configHelper->getCode('method', $code);
        }

        return $arr;
    }
}
