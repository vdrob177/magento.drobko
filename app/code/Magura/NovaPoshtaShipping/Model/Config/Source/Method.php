<?php

namespace Magura\NovaPoshtaShipping\Model\Config\Source;

/**
 * Class Method
 */
class Method extends \Magura\NovaPoshtaShipping\Model\Config\Source\Generic
{
    /**
     * Carrier code
     *
     * @var string
     */
    protected $_code = 'method';
}
