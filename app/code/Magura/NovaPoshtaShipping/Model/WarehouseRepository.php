<?php

namespace Magura\NovaPoshtaShipping\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magura\NovaPoshtaShipping\Api\Data;
use Magura\NovaPoshtaShipping\Model\ResourceModel\Warehouse as WarehouseResource;
use Magura\NovaPoshtaShipping\Model\ResourceModel\Warehouse\CollectionFactory as WarehouseCollectionFactory;
use Magura\NovaPoshtaShipping\Api\WarehouseRepositoryInterface;
use Magura\NovaPoshtaShipping\Api\Data\WarehouseSearchResultsInterfaceFactory;


/**
 * Repository for Warehouses
 * Class WarehouseRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class WarehouseRepository implements WarehouseRepositoryInterface
{
    /**
     * @var WarehouseResource
     */
    protected $resource;

    /**
     * @var Data\WarehouseInterfaceFactory
     */
    protected $warehouseFactory;

    /**
     * @var WarehouseCollectionFactory
     */
    protected $warehouseCollectionFactory;
    /**
     * @var WarehouseSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param WarehouseResource $resource
     * @param Data\WarehouseInterface $Factory
     * @param WarehouseCollectionFactory $warehouseCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        WarehouseResource $resource,
        Data\WarehouseInterfaceFactory $warehouseFactory,
        WarehouseCollectionFactory $warehouseCollectionFactory,
        WarehouseSearchResultsInterfaceFactory $warehouseResultFactory,
        CollectionProcessorInterface $collectionProcessor = null
    ) {
        $this->resource = $resource;
        $this->warehouseFactory = $warehouseFactory;
        $this->warehouseCollectionFactory = $warehouseCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $warehouseResultFactory;
    }

    /**
     * Save warehouse data
     *
     * @param Data\WarehouseInterface $warehouse
     * @return Data\WarehouseInterface
     * @throws CouldNotSaveException
     */
    public function save(\Magura\NovaPoshtaShipping\Api\Data\WarehouseInterface $warehouse)
    {
        try {
            $this->resource->save($warehouse);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save address: %1', $exception->getMessage()),
                $exception
            );
        }
        return $warehouse;
    }

    /**
     * Load Address data by given Address Identity
     *
     * @param string $Id
     * @return Data\WarehouseInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($Id)
    {
        $warehouse = $this->warehouseFactory->create();
        $this->resource->load($warehouse, $Id);
        if (!$warehouse->getId()) {
            throw new NoSuchEntityException(__('The Warehouse with the "%1" ID doesn\'t exist.', $Id));
        }
        return $warehouse;
    }



    /**
     * Delete Warehouse
     *
     * @param \Magura\NovaPoshtaShipping\Api\Data\WarehouseInterface $warehouse
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(\Magura\NovaPoshtaShipping\Api\Data\WarehouseInterface $warehouse)
    {
        try {
            $this->resource->delete($warehouse);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete address: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * Load Address data collection by given search criteria
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \Magura\NovaPoshtaShipping\Api\Data\WarehouseSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Magura\NovaPoshtaShipping\Model\ResourceModel\Warehouse\Collection $collection */
        $collection = $this->warehouseCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var Data\WarehouseSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }
    /**
     * Delete Warehouse by given Address Identity
     *
     * @param string $Id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($Id)
    {
        return $this->delete($this->getById($Id));
    }

    /**
     * Truncate table
     * @return null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function truncate()
    {
        /** @var \Magento\Framework\DB\Adapter\AdapterInterface $connection */
        $connection = $this->resource->getConnection();
        $tableName = $this->resource->getMainTable();
        $connection->truncateTable($tableName);
    }

    /**
     * Get address by field
     * @param mixed $value
     * @param string $field field to load by (defaults to model id)
     * @return \Magura\NovaPoshtaShipping\Api\Data\WarehouseInterface
     * @throws NoSuchEntityException
     */
    public function get($value, $field = null)
    {
        $warehouse = $this->warehouseFactory->create();
        $this->resource->load($warehouse, $value, $field);
        if (!$warehouse->getId()) {
            $fieldName = $field ? $field : Data\WarehouseInterface::WAREHOUSE_ID;
            throw new NoSuchEntityException(__(
                'The Warehouse with data "%1" in field "%2" doesn\'t exist.',
                $value,
                $fieldName
            ));
        }
        return $warehouse;
    }
}
