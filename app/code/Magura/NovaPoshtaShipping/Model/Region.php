<?php

namespace Magura\NovaPoshtaShipping\Model;

use Magento\Framework\Model\AbstractModel;
use Magura\NovaPoshtaShipping\Api\Data\RegionInterface;

class Region extends AbstractModel implements RegionInterface
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init(\Magura\NovaPoshtaShipping\Model\ResourceModel\Region::class);
    }
    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::REGION_ID);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @return string
     */
    public function getRef()
    {
        return $this->getData(self::REF);
    }

    /**
     * @return string
     */
    public function getAreasCenter()
    {
        return $this->getData(self::AREA);
    }

    /**
     * @param mixed $id
     * @return RegionInterface
     */
    public function setId($id)
    {
        return $this->setData(self::REGION_ID, $id);
    }

    /**
     * @param $description
     * @return RegionInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @param $ref
     * @return RegionInterface
     */
    public function setRef($ref)
    {
        return $this->setData(self::REF, $ref);
    }

    /**
     * @param $areasCenter
     * @return RegionInterface
     */
    public function setAreasCenter($areasCenter)
    {
        return $this->setData(self::AREA, $areasCenter);
    }
}
