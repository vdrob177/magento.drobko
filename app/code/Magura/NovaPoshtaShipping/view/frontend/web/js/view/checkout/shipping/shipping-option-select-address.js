;define([
    'jquery',
    'ko',
    'Magento_Ui/js/form/element/select',
    'Magento_Checkout/js/model/quote'
], function ($, ko, select, quote) {
    'use strict';

    var self;

    return select.extend({

        initialize: function () {
            self = this;
            this._super();

            $(document).on('change',"[name='select_city']",function () {

                var method = quote.shippingMethod();
                var city = $('[name="select_city"]').val();
                if (method && method['carrier_code'] !== undefined) {
                    if (method['carrier_code'] + '_' + method['method_code'] === 'novaposhta_department') {
                        self.updateDropdownValues(city);
                    }
                }

            });
        },

        /**
         * Called when selected city is changed.
         * Also called when initial selection is made.
         *
         * @returns {Object} Chainable
         */
        updateDropdownValues: function (city) {
            var valuesCollection = [];

            $.ajax({
                showLoader: true,
                url: '/rest/V1/novaposhta/warehouses?searchCriteria[filterGroups][0][filters][0][field]=city_id&searchCriteria[filterGroups][0][filters][0][value]='.concat(city),
                type: 'get',
                dataType: 'json'
            }).done(function (data) {
                data.items.forEach(function (element) {
                    var option = {
                        label: element.description,
                        value: element.id
                    };
                    valuesCollection.push(option);
                });
                self.updateDropdown(valuesCollection);
            });

        },

        /**
         * Called when option is changed in store selection list.
         * Also called when initial selection is made.
         *
         * @param value
         * @returns {Object} Chainable
         */
        updateDropdown: function (value) {
            this.setOptions(value);
        }
    });
});
