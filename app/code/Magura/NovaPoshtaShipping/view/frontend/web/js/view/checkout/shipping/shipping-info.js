;define([
    'uiComponent',
    'ko',
    'Magento_Checkout/js/model/quote'

], function (Component, ko, quote) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magura_NovaPoshtaShipping/checkout/shipping/shipping-info'
        },

        initObservable: function () {
            var self = this._super();

            this.showCourierShippingInfo = ko.computed(function () {
                var method = quote.shippingMethod();
                var selectedMethod = method != null ? method.carrier_code + '_' + method.method_code : null;

                if (method && method['carrier_code'] !== undefined) {
                    if (selectedMethod === 'novaposhta_courier') {
                        return true;
                    }
                }

                return false;

            }, this);

            this.showDepartmentShippingInfo = ko.computed(function () {
                var method = quote.shippingMethod();
                var selectedMethod = method != null ? method.carrier_code + '_' + method.method_code : null;

                if (method && method['carrier_code'] !== undefined) {
                    if (selectedMethod === 'novaposhta_department') {
                        return true;
                    }
                }

                return false;

            }, this);

            return this;
        }
    });
});
