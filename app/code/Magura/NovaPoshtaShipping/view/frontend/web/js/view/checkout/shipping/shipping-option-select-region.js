;define([
    'jquery',
    'ko',
    'Magento_Ui/js/form/element/select',
    'Magento_Checkout/js/model/quote'
], function ($, ko, select, quote) {
    'use strict';

    var self;

    return select.extend({

        initialize: function () {
            self = this;
            this._super();
            this.loadedNovaposhtaDepartmentMethod = false;

            quote.shippingMethod.subscribe(function () {

                var method = quote.shippingMethod();

                if (method && method['carrier_code'] !== undefined) {
                    if (method['carrier_code'] + '_' + method['method_code'] === 'novaposhta_department') {
                        if (!self.loadedNovaposhtaDepartmentMethod) {
                            self.loadedNovaposhtaDepartmentMethod = true;
                            self.updateDropdownValues();
                        }
                    }
                }

            }, null, 'change');
        },

        /**
         * Called when shipping method is changed.
         * Also called when initial selection is made.
         *
         * @returns {Object} Chainable
         */
        updateDropdownValues: function () {
            var valuesCollection = [];

            $.ajax({
                showLoader: true,
                url: '/rest/V1/novaposhta/regions?searchCriteria[currentPage]=1',
                type: 'get',
                dataType: 'json'
            }).done(function (data) {
                data.items.forEach(function (element) {
                    var option = {
                        label: element.description,
                        value: element.id
                    };
                    valuesCollection.push(option);
                });
                self.updateDropdown(valuesCollection);
            });


        },

        /**
         * Called when option is changed in store selection list.
         * Also called when initial selection is made.
         *
         * @param value
         * @returns {Object} Chainable
         */
        updateDropdown: function (value) {
            this.setOptions(value);
        }
    });
});
