;define([
    'jquery',
    'ko',
    'Magento_Ui/js/form/element/select',
    'Magento_Checkout/js/model/quote'
], function ($, ko, select, quote) {
    'use strict';

    var self;

    return select.extend({

        initialize: function () {
            self = this;
            this._super();

            $(document).on('change',"[name='select_region']",function () {

                var method = quote.shippingMethod();
                var region = $('[name="select_region"]').val();
                if (method && method['carrier_code'] !== undefined) {
                    if (method['carrier_code'] + '_' + method['method_code'] === 'novaposhta_department') {
                        self.updateDropdownValues(region);
                    }
                }

            });
        },

        /**
         * Called when selected shipping region is changed.
         * Also called when initial selection is made.
         *
         * @returns {Object} Chainable
         */
        updateDropdownValues: function (region) {
            var valuesCollection = [];

            $.ajax({
                showLoader: true,
                url: '/rest/V1/novaposhta/cities?searchCriteria[filterGroups][0][filters][0][field]=area_id&searchCriteria[filterGroups][0][filters][0][value]='.concat(region),
                type: 'get',
                dataType: 'json'
            }).done(function (data) {
                data.items.forEach(function (element) {
                    var option = {
                        label: element.description,
                        value: element.id
                    };
                    valuesCollection.push(option);
                });
                self.updateDropdown(valuesCollection);
            });

        },

        /**
         * Called when option is changed in store selection list.
         * Also called when initial selection is made.
         *
         * @param value
         * @returns {Object} Chainable
         */
        updateDropdown: function (value) {
            this.setOptions(value);
        }
    });
});
