;define([
    'jquery',
    'mage/utils/wrapper',
    'underscore',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, _, quote) {
    'use strict';

    return function (payloadExtender) {
        return wrapper.wrap(payloadExtender, function (originalFunction, payload) {
            var shippingRegionValue = $('[name="select_region"]') ? $('[name="select_region"]').val() : '';
            var shippingCityValue = $('[name="select_city"]') ? $('[name="select_city"]').val() : '';
            var shippingAddressValue = $('[name="select_address"]') ? $('[name="select_address"]').val() : '';

            var method = quote.shippingMethod();

            payload = originalFunction(payload);
            if (method['carrier_code'] + '_' + method['method_code'] == 'novaposhta_department') {
                _.extend(payload.addressInformation, {
                    extension_attributes: {
                        'shipping_region': shippingRegionValue,
                        'shipping_city': shippingCityValue,
                        'shipping_address': shippingAddressValue
                    }
                });
            }


            return payload;
        });
    };
});