<?php
namespace Magura\NovaPoshtaShipping\Controller\Adminhtml\Warehouses;

use Magento\Backend\App\Action;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magura\NovaPoshtaShipping\Api\Data\WarehouseInterfaceFactory
     */
    private $warehouseFactory;

    /**
     * @var \Magura\NovaPoshtaShipping\Api\WarehouseRepositoryInterface
     */
    private $warehouseRepository;

    /**
     * @param Action\Context $context
     * @param \Magura\NovaPoshtaShipping\Api\Data\WarehouseInterfaceFactory $warehouseFactory
     * @param \Magura\NovaPoshtaShipping\Api\WarehouseRepositoryInterface $warehouseRepository
     */
    public function __construct(
        Action\Context $context,
        \Magura\NovaPoshtaShipping\Api\Data\WarehouseInterfaceFactory $warehouseFactory,
        \Magura\NovaPoshtaShipping\Api\WarehouseRepositoryInterface $warehouseRepository
    ) {
        $this->warehouseFactory = $warehouseFactory;
        $this->warehouseRepository = $warehouseRepository;
        parent::__construct($context);
    }

    /**
     * Save action
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (empty($data['warehouse_id'])) {
                $data['warehouse_id'] = null;
            }
            /** @var \Magura\NovaPoshtaShipping\Model\Warehouse $model */
            $model = $this->warehouseFactory->create();

            $id = $this->getRequest()->getParam('warehouse_id');
            if ($id) {
                try {
                    $model = $this->warehouseRepository->getById($id);
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                    $this->messageManager->addErrorMessage(__('This warehouse no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }
            $model->setData($data);

            try {
                $this->warehouseRepository->save($model);
                if ($id) {
                    $this->messageManager->addSuccessMessage(__('You saved edited Warehouse.'));
                } else {
                    $this->messageManager->addSuccessMessage('You saved a Warehouse.');
                }
                $this->_objectManager->get(\Magento\Backend\Model\Session::class)->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['warehouse_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\CouldNotSaveException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the address.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['warehouse_id' => $this->getRequest()->getParam('warehouse_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
