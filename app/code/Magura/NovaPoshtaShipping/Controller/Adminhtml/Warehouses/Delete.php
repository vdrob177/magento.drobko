<?php
namespace Magura\NovaPoshtaShipping\Controller\Adminhtml\Warehouses;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magura\NovaPoshtaShipping\Api\WarehouseRepositoryInterface;

class Delete extends Action
{
    /**
     * Warehouses repository
     *
     * @var WarehouseRepositoryInterface
     */
    protected $warehouseRepository;

    /**
     * Upload constructor.
     *
     * @param Context $context
     * @param WarehouseRepositoryInterface $warehouseRepository
     */
    public function __construct(
        Context $context,
        WarehouseRepositoryInterface $warehouseRepository
    ) {
        $this->warehouseRepository = $warehouseRepository;
        parent::__construct($context);
    }
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('warehouse_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $this->warehouseRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The warehouse has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['warehouse_id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a warehouse to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
