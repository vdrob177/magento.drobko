<?php
namespace Magura\NovaPoshtaShipping\Controller\Adminhtml\Regions;

use Magura\NovaPoshtaShipping\Api\RegionRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

class Delete extends Action
{
    /**
     * Region repository
     *
     * @var RegionRepositoryInterface
     */
    protected $regionRepository;

    /**
     * Upload constructor.
     *
     * @param Context $context
     * @param RegionRepositoryInterface $regionRepository
     */
    public function __construct(
        Context $context,
        RegionRepositoryInterface $regionRepository
    ) {
        $this->regionRepository = $regionRepository;
        parent::__construct($context);
    }
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('region_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $this->regionRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The region has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['region_id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a region to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
