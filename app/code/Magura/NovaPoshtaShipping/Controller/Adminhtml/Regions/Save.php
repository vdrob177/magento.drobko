<?php
namespace Magura\NovaPoshtaShipping\Controller\Adminhtml\Regions;

use Magento\Backend\App\Action;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magura\NovaPoshtaShipping\Api\Data\RegionInterfaceFactory
     */
    private $regionFactory;

    /**
     * @var \Magura\NovaPoshtaShipping\Api\RegionRepositoryInterface
     */
    private $regionRepository;

    /**
     * @param Action\Context $context
     * @param \Magura\NovaPoshtaShipping\Api\Data\RegionInterfaceFactory $regionFactory
     * @param \Magura\NovaPoshtaShipping\Api\RegionRepositoryInterface $regionRepository
     */
    public function __construct(
        Action\Context $context,
        \Magura\NovaPoshtaShipping\Api\Data\RegionInterfaceFactory $regionFactory,
        \Magura\NovaPoshtaShipping\Api\RegionRepositoryInterface $regionRepository
    ) {
        $this->regionFactory = $regionFactory;
        $this->regionRepository = $regionRepository;
        parent::__construct($context);
    }

    /**
     * Save action
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (empty($data['region_id'])) {
                $data['region_id'] = null;
            }
            /** @var \Magura\NovaPoshtaShipping\Model\Region $model */
            $model = $this->regionFactory->create();

            $id = $this->getRequest()->getParam('region_id');
            if ($id) {
                try {
                    $model = $this->regionRepository->getById($id);
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                    $this->messageManager->addErrorMessage(__('This region no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }
            $model->setData($data);

            try {
                $this->regionRepository->save($model);
                if ($id) {
                    $this->messageManager->addSuccessMessage(__('You saved edited Region.'));
                } else {
                    $this->messageManager->addSuccessMessage('You saved a Region.');
                }
                $this->_objectManager->get(\Magento\Backend\Model\Session::class)->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['region_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\CouldNotSaveException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the region.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['region_id' => $this->getRequest()->getParam('region_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
