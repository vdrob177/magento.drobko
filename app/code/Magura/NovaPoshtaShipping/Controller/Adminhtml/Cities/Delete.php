<?php
namespace Magura\NovaPoshtaShipping\Controller\Adminhtml\Cities;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magura\NovaPoshtaShipping\Api\CityRepositoryInterface;

class Delete extends Action
{
    /**
     * City repository
     *
     * @var CityRepositoryInterface
     */
    protected $cityRepository;

    /**
     * Upload constructor.
     *
     * @param Context $context
     * @param CityRepositoryInterface $cityRepository
     */
    public function __construct(
        Context $context,
        CityRepositoryInterface $cityRepository
    ) {
        $this->cityRepository = $cityRepository;
        parent::__construct($context);
    }
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('city_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $this->cityRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The city has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['city_id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a city to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
