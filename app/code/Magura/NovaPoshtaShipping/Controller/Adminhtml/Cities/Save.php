<?php
namespace Magura\NovaPoshtaShipping\Controller\Adminhtml\Cities;

use Magento\Backend\App\Action;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magura\NovaPoshtaShipping\Api\Data\CityInterfaceFactory
     */
    private $cityFactory;

    /**
     * @var \Magura\NovaPoshtaShipping\Api\CityRepositoryInterface
     */
    private $cityRepository;

    /**
     * @param Action\Context $context
     * @param \Magura\NovaPoshtaShipping\Api\Data\CityInterfaceFactory $cityFactory
     * @param \Magura\NovaPoshtaShipping\Api\CityRepositoryInterface $cityRepository
     */
    public function __construct(
        Action\Context $context,
        \Magura\NovaPoshtaShipping\Api\Data\CityInterfaceFactory $cityFactory,
        \Magura\NovaPoshtaShipping\Api\CityRepositoryInterface $cityRepository
    ) {
        $this->cityFactory = $cityFactory;
        $this->cityRepository = $cityRepository;
        parent::__construct($context);
    }

    /**
     * Save action
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (empty($data['city_id'])) {
                $data['city_id'] = null;
            }
            /** @var \Magura\NovaPoshtaShipping\Model\City $model */
            $model = $this->cityFactory->create();

            $id = $this->getRequest()->getParam('city_id');
            if ($id) {
                try {
                    $model = $this->cityRepository->getById($id);
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                    $this->messageManager->addErrorMessage(__('This city no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }
            $model->setData($data);

            try {
                $this->cityRepository->save($model);
                if ($id) {
                    $this->messageManager->addSuccessMessage(__('You saved edited City.'));
                } else {
                    $this->messageManager->addSuccessMessage('You saved a City.');
                }
                $this->_objectManager->get(\Magento\Backend\Model\Session::class)->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['city_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\CouldNotSaveException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the city.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['city_id' => $this->getRequest()->getParam('city_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
