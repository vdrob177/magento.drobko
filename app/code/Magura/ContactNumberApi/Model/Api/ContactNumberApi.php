<?php

namespace Magura\ContactNumberApi\Model\Api;

class ContactNumberApi
{
    const STORE_PHONE_NUMBER_CONFIGURATION = 'general/store_information/phone';
    protected $scopeConfig;
    protected $_configWriter;

    /**
     * ContactNumber constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Config\ConfigResource\ConfigInterface  $resourceConfig
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->_configWriter = $resourceConfig;
    }

    /**
     * @inheritdoc
     */
    public function getStorePhone()
    {
        return $this->scopeConfig->getValue(
            self::STORE_PHONE_NUMBER_CONFIGURATION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return false|string
     */
    public function getNumber()
    {
        $response = ['success' => false];
        try {
            $response = ['success' => true, 'Store Phone Number' => $this->getStorePhone()];
        } catch (\Exception $e) {
            $response = ['success' => false, 'Store Phone Number' => $e->getMessage()];
        }
        $returnArray = json_encode($response);
        return $returnArray;
    }

    /**
     * @param string $param
     * @return string
     */
    public function setNumber($param)
    {
        $this->_configWriter->saveConfig(
            self::STORE_PHONE_NUMBER_CONFIGURATION,
            $param,
            \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            \Magento\Store\Model\Store::DEFAULT_STORE_ID
        );
        return $this->getNumber();
    }
}
