<?php

namespace Magura\ContactNumberApi\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class ChangeContactNumberResolver implements ResolverInterface
{
    const STORE_PHONE_NUMBER_CONFIGURATION = 'general/store_information/phone';
    protected $_configWriter;

    public function __construct(
        \Magento\Framework\App\Config\ConfigResource\ConfigInterface $resourceConfig
    ) {
        $this->_configWriter = $resourceConfig;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $this->_configWriter->saveConfig(
            self::STORE_PHONE_NUMBER_CONFIGURATION,
            $args['param'],
            \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            \Magento\Store\Model\Store::DEFAULT_STORE_ID
        );
        return $args['param'];
    }
}
