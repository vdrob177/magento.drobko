<?php


namespace Magura\ContactNumberApi\Api;


interface ContactNumberInterface
{

    /**
     * GET for Post api
     * @return string
     */
    public function getNumber();
    /**
     * @return mixed
     */
    public function getStorePhone();

    /**
     * @param string $param
     * @return string
     */
    public function setNumber($param);
}
