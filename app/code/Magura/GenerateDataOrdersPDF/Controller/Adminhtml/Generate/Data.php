<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magura\GenerateDataOrdersPDF\Controller\Adminhtml\Generate;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magura\GenerateDataOrdersPDF\Model\Pdf\OrdersDataPdf;

class Data extends \Magento\Backend\App\Action
{
    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var DateTime
     */
    protected $dateTime;

    /**
     * @var OrdersDataPdf
     */
    protected $pdf;
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Data constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param DateTime $dateTime
     * @param FileFactory $fileFactory
     * @param OrdersDataPdf $pdf
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        DateTime $dateTime,
        FileFactory $fileFactory,
        OrdersDataPdf $pdf,
        CollectionFactory $collectionFactory
    )
    {
        $this->fileFactory = $fileFactory;
        $this->dateTime = $dateTime;
        $this->pdf = $pdf;
        $this->collectionFactory = $collectionFactory;

        parent::__construct($context);
    }

    public function execute()
    {
        $collection = $this->collectionFactory->create();
        $items = $collection->getItems();
        $pdf = $this->pdf->getPdf($items);
        $fileContent = ['type' => 'string', 'value' => $pdf->render(), 'rm' => true];

        return $this->fileFactory->create(
            sprintf('DataOrders%s.pdf', $this->dateTime->date('Y-m-d_H-i-s')),
            $fileContent,
            DirectoryList::VAR_DIR,
            'application/pdf'
        );
    }
}
