<?php

namespace Magura\SendIdProducts\Plugin;

use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote;
use Magura\SendIdProducts\Helper\Email;

class Cart
{
    /**
     * @var Quote
     */
    protected $quote;
    private $helperEmail;

    /**
     * Plugin constructor.
     *
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        Email $helperEmail
    ) {
        $this->quote = $checkoutSession->getQuote();
        $this->helperEmail = $helperEmail;
    }

    /**
     * beforeAddProduct
     *
     * @param      $subject
     * @param      $productInfo
     * @param null $requestInfo
     *
     * @return array
     * @throws LocalizedException
     */
    public function beforeAddProduct($subject, $productInfo, $requestInfo = null)
    {
        $this->helperEmail->sendEmail($productInfo->getId());
        return [$productInfo, $requestInfo];
    }
}
