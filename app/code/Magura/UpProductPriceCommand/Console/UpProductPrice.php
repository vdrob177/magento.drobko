<?php

namespace Magura\UpProductPriceCommand\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpProductPrice extends Command
{
    const PRECENT1 = 'precent1';
    const PRECENT2 = 'precent2';
    protected $productResourceModel;
    protected $productFactory;
    protected $_storeManager;

    /**
     * UpProductPrice constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Product $productResourceModel
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product $productResourceModel,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\App\State $state
    )
    {
        $this->productResourceModel = $productResourceModel;
        $this->productFactory = $productFactory;
        $this->_storeManager = $storeManager;
        try {
            $state->setAreaCode('adminhtml');
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            // Intentionally left empty.
            echo $e->getMessage();
            exit;
        }
        parent::__construct();
    }

    protected function configure()
    {
        $options = [
            new InputOption(
                self::PRECENT1,
                null,
                InputOption::VALUE_OPTIONAL,
                'Precent1'
            ),
            new InputOption(
                self::PRECENT2,
                null,
                InputOption::VALUE_OPTIONAL,
                'Precent2'
            )
        ];
        $this->setName('product:up-price');
        $this->setDescription('Up price for 10 first products');
        $this->setDefinition($options);

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $precent = $input->getOption(self::PRECENT1) ?? 10;
        $precent2 = $input->getOption(self::PRECENT2) ?? 10;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productFactory = $objectManager->get('\Magento\Catalog\Model\ProductFactory');
        $output->writeln('20 first products on '.$precent.' next 10 products on '.$precent2);
        for ($i = 1; $i < 30; $i++) {
            $product = $productFactory->create()->load($i);
            if ($i <= 20)
                $addPrice = $product->getPrice() * ($precent / 100);
            else
                $addPrice = $product->getPrice() * ($precent2 / 100);
            $product->setPrice($product->getPrice() + $addPrice);
            $output->writeln($product->getName() . ' -> ' . $product->getPrice().'(+'.$addPrice.')');
            $product->save();
            unset($product);
        }
    }
}
